import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from '../Pages/Login/Login';
import HomePage from '../Views/HomePage/HomePage';

const MainRoute = () => {
  return (
    <BrowserRouter>
      <Switch>
      
      <Route exact path='/' component={HomePage}></Route>
        <Route exact path='/login' component={Login} />

      </Switch>
    </BrowserRouter>
  );
};
export default MainRoute;

