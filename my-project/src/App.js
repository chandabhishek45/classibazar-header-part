import React from 'react';
import './App.css';
import Header from './components/Header';
import Nav from './components/Nav';


import MainRoute from "./Routes/MainRoute";
import { BrowserRouter as Router} from 'react-router-dom';



function App () {

  return (
    <div className="App">
      <Router>
      <section id="header">
        <Header />
      </section>
      <section id="nav">
        <Nav />
      </section>
       <MainRoute />
      </Router>
    </div>
  );
}

export default App;

