import React from 'react'
import "./Header.css";
import { Link } from "react-router-dom";




function Header () {


    function refreshPage(){ 
        setTimeout(()=>{
            window.location.reload(false);
        }, 500);
        console.log('page to reload')
        }
    
    
  
 
    return (

    <div className='header'>

        <Link to='/'>
            <img className='header__logo' onClick={refreshPage}
            src="https://deals.classibazaar.com.au/assets/images/classi-logo.png" 
            alt=""/>
        </Link>
        <div className = 'input'>
        <input className="header__searchInput" type="text" placeholder="Search all the products here..." />
        <button className="header__searchIcon">SEARCH</button>
        </div>    

        <Link to='/login'>
        <div className='login-button'>
        <button className='login_button' onClick={refreshPage}>
          <span>Login</span>  
        </button>
        </div>

        </Link>

        <div className='location'>
            <button className='location_button'><span>Austrilia</span></button>
        </div>


        </div>
    )
}


    
export default Header;
