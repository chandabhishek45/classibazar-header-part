import React from "react";
import "./Nav.css";
import { Link } from 'react-router-dom';
import { Button } from "@material-ui/core";





function Nav() {

    function refreshPage(){ 
        setTimeout(()=>{
            window.location.reload(false);
        }, 500);
        console.log('page to reload')
        }
    

    return (
        <div className="nav">
            <Link to='/holiday_travel'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small',  }}
            >Holiday & Travel</Button>
            </Link>

            <Link to='/things_to_do'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small',}}
            >Things to do</Button>
            </Link>

            <Link to='/health_beauty'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small', }}
            >Health & Beauty</Button>
            </Link>

            <Link to='/resturant_food'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small', }}
            >Resturant & Food</Button>
            </Link>

            <Link to='/shopping'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small', }}
            >Shopping</Button>
            </Link>

            <Link to='/service'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small', }}
            >Services</Button>
            </Link>

            <Link to='/youwillbefine'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'small', }}
            >#youwillbefine</Button>
            </Link>

            <Link to='/comestic_products'>
            <Button type="button" onClick={refreshPage}
            style={{maxWidth: '150px', maxHeight: '50px', minWidth: '150px', minHeight: '50px', fontSize: 'smaller', }}
            >Cosmetic Products</Button>
            </Link>
        </div>
    );
}

export default Nav