import CarouselContainer from '../../carousel/CarouselContainer';
import Items from '../../items/Items';

const HomePage = () => {
  return (
    <div>
      <div className="App">
        <section id="carousel">
          <CarouselContainer />
        </section>
        <section id="items">
          <Items />
        </section>

      </div>
    </div>
  );
};
export default HomePage;
